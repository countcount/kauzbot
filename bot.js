#!/usr/bin/env node

const eris = require('eris');
const mysql = require('mysql');
var auth = require('./auth.json');

const PREFIX = '!';

const con = mysql.createConnection({
    host: 'localhost',
    user: 'kauzbot',
    password: auth.mysqlpw,
    database: 'kauzbot',
    supportBigNumbers: true
});

con.connect((err) => {
    if(err){
        console.log('Error connecting to Db');
        return;
    }
    console.log('Connection established');
});

// Create a Client instance with our bot token.
const bot = new eris.Client(auth.token);

const commandForName = {};

// TOWNSTATUS SETZEN
commandForName['townstart'] = (msg, args) => {
    let timer = args[0];

    let channel = msg.channel.id;

    con.query('INSERT INTO townstatus VALUES ("' + channel + '", 1) ON DUPLICATE KEY UPDATE status = 1', (err,rows) => {
        if(err) throw err;
        console.log('Data received from Db:\n');
        console.log(rows);
    });

    return msg.channel.createMessage('Neue Stadt? Super! Ich werde ich Euch assistieren.');
};
commandForName['townend'] = (msg, args) => {
    let timer = args[0];

    let channel = msg.channel.id;

    con.query('INSERT INTO townstatus VALUES ("' + channel + '", 0) ON DUPLICATE KEY UPDATE status = 0', (err,rows) => {
        if(err) throw err;
        console.log('Data received from Db:\n');
        console.log(rows);
    });

    return msg.channel.createMessage('Oh, Stadt zu Ende? Dann gibt es keine regelmäßigen Erinnerungen mehr von mir.');
};

// BUDDELTIMER SETZEN
commandForName['buddel'] = (msg, args) => {
    let timer = args[0];

    if (timer === undefined || isNaN(timer)) {
        timer = 120;
    }
    let ts = Math.round((new Date()).getTime() / 1000);
    let over = ts + (timer * 60);
    let nick = msg.author.username;
    let userid = msg.author.id;
    let channel = msg.channel.id;

    con.query('INSERT INTO buddeltimer VALUES (NULL, ' + over + ', "' + channel + '", "' + nick + '", ' + userid + ', 0)', (err,rows) => {
        if(err) throw err;
        console.log('Data received from Db:\n');
        console.log(rows);
    });

    return msg.channel.createMessage('Alles klar ' + nick + ', ich werde Dich erinnern.');
};

// KEKS VERTEILEN
commandForName['keks'] = (msg, args) => {
    let mention = args[0];

    if (mention === "all" || mention === "alle" || mention === "allen") {
        mention = "alle Anwesenden";
    }
    if (mention === undefined) {
        mention = msg.author.username;
    }
    //const amount = parseFloat(args[1]);
    const keks = [
        'einen riesen Schoki-Knusperkeks',
        'ein Chocolate Chip Cookie',
        'ein Waffelröllchen',
        'einen Butterkeks'
    ];

    // TODO: Handle invalid command arguments, such as:
    // 1. No mention or invalid mention.
    // 2. No amount or invalid amount.
    let rand = Math.floor(Math.random() * keks.length);
    return msg.channel.createMessage('Ich habe ' + keks[rand] + ' frisch für ' + mention + ' gebacken.');
};

// EIS VERTEILEN
commandForName['eis'] = (msg, args) => {
    let mention = args[0];

    if (mention === "all" || mention === "alle" || mention === "allen") {
        mention = "alle Anwesenden";
    }
    if (mention === undefined) {
        mention = msg.author.username;
    }
    //const amount = parseFloat(args[1]);
    const eis = [
        'Ein Flutschfinger',
        'Leckeres Sanddornsofteis',
        'Ein Nogger',
        'Einmal Bananensplit',
        'Der Riesenfruchtbecher ist',
        'Eine Eisbombe Spezial'
    ];

    // TODO: Handle invalid command arguments, such as:
    // 1. No mention or invalid mention.
    // 2. No amount or invalid amount.
    let rand = Math.floor(Math.random() * eis.length);
    return msg.channel.createMessage('Lust auf etwas Kaltes? ' + eis[rand] + ' für ' + mention + '.');
};

// KNÜPPEL VERTEILEN
commandForName['knüppel'] = (msg, args) => {
    let mention = args[0];

    if (mention === "all" || mention === "alle" || mention === "allen") {
        return msg.channel.createMessage('Ein Knüppel für alle Anwesenden? Das wird nicht reichen.');
    }
    if (mention === undefined) {
        mention = msg.author.username;
    }
    let msgs = [
        'Siehst Du diesen tollen Knüppel, ' + mention + '? Damit zieh ich Dir jetzt einen über den Schädel!',
        'Eine Kopfnuss für ' + mention + ', zwei Kopfnüsse für ' + mention + ', drei Kopfnüsse für ' + mention + '.',
        'Knüppel aus dem Sack! Und immer druff uff ' + mention + '!'
    ];
    let rand = Math.floor(Math.random() * msgs.length);
    return msg.channel.createMessage(msgs[rand]);
};

// POPCORN VERTEILEN
commandForName['popcorn'] = (msg, args) => {
    let mention = args[0];

    if (mention === "all" || mention === "alle" || mention === "allen") {
        return msg.channel.createMessage('Popcorn für alle!');
    }
    if (mention === undefined) {
        mention = msg.author.username;
    }
    let msgs = [
        'Whoa! Hab ich was verpasst? Hier, ' + mention + ', ich habe jedenfalls frisches Popcorn.',
        'Für Popcorn bin ich immer zu haben. Ein Eimer für ' + mention + ', ein Eimer für mich.',
        'An alle: Macht ruhig weiter so! ' + mention + ' und ich haben Popcorn und schauen zu.'
    ];
    let rand = Math.floor(Math.random() * msgs.length);
    return msg.channel.createMessage(msgs[rand]);
};

// KAFFEE VERTEILEN
commandForName['kaffee'] = (msg, args) => {
    let mention = args[0];

    if (mention === "all" || mention === "alle" || mention === "allen") {
        mention = "alle";
    }
    if (mention === undefined) {
        mention = msg.author.username;
    }
    //const amount = parseFloat(args[1]);
    const kaffee = [
        'ein riesen Pott Kaffee',
        'ein Cappuccino',
        'ein Latte Macchiato',
        'ein Plörre, die sich koffeinfrei schimpft'
    ];

    // TODO: Handle invalid command arguments, such as:
    // 1. No mention or invalid mention.
    // 2. No amount or invalid amount.
    let rand = Math.floor(Math.random() * kaffee.length);
    return msg.channel.createMessage('Müde, ' + mention + '? Frisch gebrüht: ' + kaffee[rand] + '!');
};

// BIER VETEILEN
commandForName['bier'] = (msg, args) => {
    let mention = args[0];

    if (mention === "all" || mention === "alle" || mention === "allen") {
        mention = "alle Anwesenden";
    }
    if (mention === undefined) {
        mention = msg.author.username;
    }
    //const amount = parseFloat(args[1]);
    const bier = [
        'eine Flasche Vodka Marinostov',
        'eine Flasche Wake the Dead',
        'ein Eisengefäß mit modrigem Wasser',
        'eine Wasserflasche',
        'eine grüne Bierflasche',
        'einen Krug Bier',
        'ein Schokobier',
        'eine Apfelsaftschorle',
        'eine Bloody Mary',
        'einen ZOMBIE',
        'ein Bier und einen Kurzen'
    ];

    // TODO: Handle invalid command arguments, such as:
    // 1. No mention or invalid mention.
    // 2. No amount or invalid amount.
    let rand = Math.floor(Math.random() * bier.length);
    return msg.channel.createMessage('Hm, lecker, ' + bier[rand] + ' für ' + mention + '.');
};

// PING PONG
commandForName['ping'] = (msg) => {
    //console.log(msg);
    return msg.channel.createMessage(`pong`);
};

// ZOMBIES BERECHNEN
commandForName['z'] = (msg, args) => {
    if (args.length < 4) {
        return msg.channel.createMessage('@' + msg.author.username + ' Du musst mir schon mindestens die vier Zahlen für die Berechnung geben.');
    }
    else {
        let minmin = parseInt(args[0]);
        let maxmax = parseInt(args[1]);
        let maxmin = parseInt(args[2]);
        let minmax = parseInt(args[3]);
        let z = Math.ceil(Math.abs(((minmin * minmax) - (maxmax * maxmin)) / (maxmin - minmin + maxmax - minmax)));
        return msg.channel.createMessage('Laut den vorliegenden Zahlen werden ' + z + ' Zombies die Stadt angreifen.');
    }
};

// When the bot is connected and ready, log to console.
bot.on('ready', () => {
    console.log('Connected and ready.');
});

// Every time a message is sent anywhere the bot is present,
// this event will fire and we will check if the bot was mentioned.
// If it was, the bot will attempt to respond with "Present".
bot.on('messageCreate', async (msg) => {
    const botWasMentioned = msg.mentions.find(
        mentionedUser => mentionedUser.id === bot.user.id,
    );
    const content = msg.content;

    // Ignore any message that doesn't start with the correct prefix.
    if (!content.startsWith(PREFIX)) {
        return;
    }

    if (botWasMentioned) {
        try {
            await msg.channel.createMessage('Present');
        } catch (err) {
            // There are various reasons why sending a message may fail.
            // The API might time out or choke and return a 5xx status,
            // or the bot may not have permission to send the
            // message (403 status).
            console.warn('Failed to respond to mention.');
            console.warn(err);
        }
    }

    // Extract the parts of the command and the command name
    const parts = content.split(' ').map(s => s.trim()).filter(s => s);
    const commandName = parts[0].substr(PREFIX.length);

    // Get the appropriate handler for the command, if there is one.
    const commandHandler = commandForName[commandName];
    if (!commandHandler) {
        return;
    }

    // Separate the command arguments from the command prefix and command name.
    const args = parts.slice(1);

    try {
        // Execute the command.
        await commandHandler(msg, args);
    } catch (err) {
        console.warn('Error handling command');
        console.warn(err);
    }
});

bot.on('error', err => {
    console.warn(err);
});

bot.connect();

bot.timer = () => {
    con.query('SELECT * FROM buddeltimer WHERE `over` < UNIX_TIMESTAMP() AND stamp = 0', (err,rows) => {
        if(err) throw err;
        //console.log('Data received from Db:\n');
        //console.log(rows);

        for (let i = 0; i < rows.length; i++) {
            let row = rows[i];
            //console.log(row.userid);
            //console.log(bot.getChannel(row.channel));
            bot.getChannel(row.channel).createMessage('<@' + row.userid + '> Deine Grabung ist vorbei.');

            con.query('UPDATE buddeltimer SET stamp = 1 WHERE id = ' + row.id, (err,rows) => {
                if(err) throw err;
            });
        }
    });
    let tzTime = new Date().toLocaleString("en-US", {timeZone: "Europe/Berlin"});
    let current_date = new Date(tzTime);
    let current_hour = current_date.getHours();
    let current_mins = current_date.getMinutes();
    let current_secs = current_date.getSeconds();

    con.query('SELECT * FROM townstatus WHERE status = 1', (err,rows) => {
        if(err) throw err;
        //console.log('Data received from Db:\n');
        //console.log(rows);

        for (let i = 0; i < rows.length; i++) {
            let row = rows[i];
            if (current_hour == 22 && current_mins == 45 && current_secs <= 30) {
                bot.getChannel(row.channel).createMessage('Gleich wird die Gitarre gespielt, wer jetzt noch draußen ist, sollte mal langsam reinkommen.');
            }
            if (current_hour == 23 && current_mins == 40 && current_secs <= 30) {
                bot.getChannel(row.channel).createMessage('Ich hoffe, es sind alle, die nicht campen, in der Stadt. Kann das Tor zu?');
            }
            if (current_hour == 0 && current_mins == 21 && current_secs <= 30) {
                bot.getChannel(row.channel).createMessage('Der Angriff ist vorbei. Los, geht buddeln!');
            }
        }
    });

    //console.log('Es ist ' + current_hour + ':' + current_mins);
    //bot.getChannel("666257264642097154").createMessage('Es ist ' + current_hour + ':' + current_mins);
};


setInterval(function() {
    bot.timer();
}, 30000);
